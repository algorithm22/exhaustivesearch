/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author Windows10
 */
public class ExhaustiveSearch {
    private int[] input;
    static int indexArr = 0;
    static ArrayList<ArrayList<Integer>> Arr1 = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> Arr2 = new ArrayList<ArrayList<Integer>>();

    public ExhaustiveSearch(int[] input) {
        this.input = input;
    }

    public void getDisjointArray1() {
        for (int i = 0; i < Arr1.size(); i++) {
            for (int j = 0; j < Arr1.get(i).size(); j++) {
                System.out.print(Arr1.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void getDisjointArray2() {
        for (int i = 0; i < Arr2.size(); i++) {
            for (int j = 0; j < Arr2.get(i).size(); j++) {
                System.out.print(Arr2.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    static void prepareArray(int arr[], int n, int r) {
        int data[] = new int[r];
        combinationUtil(arr, data, 0, n - 1, 0, r);
    }

    static void combinationUtil(int arr[], int data[], int start,
            int end, int index, int r) {
        ArrayList<Integer> temp = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                temp.add(data[j]);
            }
            Arr1.add(indexArr, temp);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            combinationUtil(arr, data, i + 1, end, index + 1, r);
        }
    }

    public void sum() {
        for (int i = 0; i < Arr1.size(); i++) {
            if (Arr1.contains(Arr2.get(i))) {
                Arr2.remove(i);
                Arr1.remove(i);
            }
        }
        for (int i = 0; i < Arr1.size(); i++) {
            int sumA = 0;
            int sumB = 0;
            for (int j = 0; j < Arr1.get(i).size(); j++) {
                sumA += Arr1.get(i).get(j);
            }
            for (int j = 0; j < Arr2.get(i).size(); j++) {
                sumB += Arr2.get(i).get(j);
            }
            if (sumA == sumB) {
                System.out.println(Arr1.get(i) + " " + Arr2.get(i));
            }
        }
    }

    public void process() {
        ArrayList<Integer> temp = new ArrayList<>();
        ArrayList<Integer> temp2 = new ArrayList<>();

        for (int o = 0; o < input.length; o++) {
            temp.add(input[o]);
        }
        for (int i = 0; i < input.length; i++) {
            temp2.add(input[i]);
        }

        int arr[] = input;
        int n = arr.length;

        for (int i = input.length; i >= 0; i--) {
            prepareArray(arr, n, i);
        }

        indexArr = 1;
        Arr2.add(0, temp2);
        for (int j = 1; j < Arr1.size(); j++) {
            temp = new ArrayList<>();
            for (int i = 0; i < input.length; i++) {
                temp.add(input[i]);
            }
            for (int k = 0; k < Arr1.get(j).size(); k++) {
                for (int u = 0; u < temp.size(); u++) {
                    if (temp.get(u) == Arr1.get(j).get(k)) {
                        temp.remove(u);
                        u--;
                    }
                }
            }
            Arr2.add(indexArr, temp);
            indexArr++;
        }
    }
}
