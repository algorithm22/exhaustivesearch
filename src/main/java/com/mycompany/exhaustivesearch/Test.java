/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.exhaustivesearch;

/**
 *
 * @author Windows10
 */
public class Test {
     public static void main(String[] args) {
        int[] test = {1, 9, 8, 0, 10, 20, 30};
        showInput(test);
        ExhaustiveSearch ex = new ExhaustiveSearch(test);
        ex.process();
        System.out.println("------------------------------------");
        ex.getDisjointArray1();
        System.out.println("------------------------------------");
        ex.getDisjointArray2();
        ex.sum();

    }

    private static void showInput(int[] test) {
        System.out.print("Input: ");
        for (int i = 0; i < test.length; i++) {
            System.out.print(test[i] + " ");
        }
        System.out.println("");
    }
}
